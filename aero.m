% -------------------------------------------------------------------------
% Title:  Aerodynamics Model
% Author: Jordan Street
% -------------------------------------------------------------------------
% Description: This function uses lifting line approximations to calculate
% CL and CD as a function of angle of attack.
%
% Inputs:
%     alpha - angle of attack (rad)
%
% Outputs:
%     CL    - lift coefficient
%     CD    - drag coefficient
% -------------------------------------------------------------------------

function [CL, CD] = aero(alpha)
    % Aerodynamic parameters
    CLa = 2*pi; % lift curve slope
    CD0 = 0.02; % base drag coefficient
    AR  = 5;    % aspect ratio
    e   = 0.7;  % efficiency factor
    
    % Lift coefficient
    CL = CLa*AR/(AR + 2)*alpha;
    
    % Drag coefficient
    CD = CD0 + CL.^2/(pi*AR*e);
end
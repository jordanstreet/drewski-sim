clear
clc

%% Setup
% Simulation settings
tMax    = 60;         % max simulation time (sec)
simRate = 10;         % simulation rate (hz)
alpha   = deg2rad(0); % angle of attack command (rad)

% Derived settings
nt = tMax*simRate + 1; % number of points in simulation history
dt = 1/simRate;        % simulation delta-time

% Missile history
% TODO: log: V, gamma, x, z, alpha
% TODO: missile.<variable> = nan(nt, 1);

% Missile initial conditions
% TODO: specify V(1), gamma(1), x(1), z(1)
% TODO: missile.<variable>(1) = <initial condiiton>

%% Run simulation
% Simulation time vector
t = 0:dt:tMax;

% Iterate through time
for i=1:nt - 1
    % Log missile angle of attack
    % TODO: log missile alpha
    
    % Calculate state derivatives
    % TODO: calculate state derivatives using eom function
    
    % Composite state vector
    % TODO: create vector of state variables and vector of derivatives
    
    % Euler integration
    % TODO: integrate state variables
    
    % Log missile variables
    % TODO: missile.<variable>(i+1) = <new value>
    
    % End the simulation if we've hit the ground
    % TODO: check missile height and use "break;" to end simulation
end

%% Plot results
% TODO: all plots should have a grid and axis labels and should call "hold
% on" to be able to overlay different scenarios

% TODO: plot altitude vs downrange position in the top left corner

% TODO: plot velocity vs time in the top right corner

% TODO: plot flight path angle in degrees vs time  in the bottom left
% corner

% TODO: plot angle of attack in degrees vs time in the bottom right corner
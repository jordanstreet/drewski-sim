% -------------------------------------------------------------------------
% Title:  Atmosphere Model
% Author: Jordan Street
% -------------------------------------------------------------------------
% Description: This function calculates atmosphere parameters as a function
% of altitude for a standard day atmosphere.
%
% Inputs:
%     h   - altitude (ft)
%
% Outputs:
%     a   - speed of sound (ft/s)
%     rho - air density (slug/ft^3)
% -------------------------------------------------------------------------

function [a, rho] = atm(h)
    % Convert altitude to meters for 'atmoscoesa' call
    h = convlength(h, 'ft', 'm');
    
    % Call 'atmoscoesa' for atmosphere parameters
    [~, a, ~, rho] = atmoscoesa(h);
    
    % Convert parameters to english units
    a   = convvel(a, 'm/s', 'ft/s');
    rho = convdensity(rho, 'kg/m^3', 'slug/ft^3');
end
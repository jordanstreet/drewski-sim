clear
clc

%% Setup
% Simulation settings
tMax    = 60;         % max simulation time (sec)
simRate = 10;         % simulation rate (hz)
alpha   = deg2rad(0); % angle of attack command (rad)

% Derived settings
nt = tMax*simRate + 1; % number of points in simulation history
dt = 1/simRate;        % simulation delta-time

% Missile history
missile.V     = nan(nt, 1);
missile.gamma = nan(nt, 1);
missile.x     = nan(nt, 1);
missile.z     = nan(nt, 1);
missile.alpha = nan(nt, 1);

% Missile initial conditions
missile.V(1)     =  500;  % initial velocity (ft/s)
missile.gamma(1) =  0;    % initial flight path angle (rad)
missile.x(1)     =  0;    % initial position [x] (ft)
missile.z(1)     = -1000; % initial position [z] (ft)

%% Run simulation
% Simulation time vector
t = 0:dt:tMax;

% Iterate through time
for i=1:nt - 1
    % Log missile angle of attack
    missile.alpha(i) = alpha;
    
    % Calculate state derivatives
    [Vdot, gammaDot, xDot, zDot] = ...
        eom(missile.V(i), missile.gamma(i), missile.z(i), alpha);
    
    % Composite state vector
    states      = [missile.V(i), missile.gamma(i), missile.x(i), missile.z(i)];
    stateDerivs = [Vdot,         gammaDot,         xDot,         zDot        ];
    
    % Euler integration
    states = states + stateDerivs*dt;
    
    % Log missile variables
    missile.V(i+1)     = states(1);
    missile.gamma(i+1) = states(2);
    missile.x(i+1)     = states(3);
    missile.z(i+1)     = states(4);
    
    % End the simulation if we've hit the ground
    if missile.z(i+1) > 0
        break;
    end
end

%% Plot results
% Altitude vs downrange position
subplot(2,2,1);
plot(missile.x, -missile.z, 'linewidth', 2);
grid on; axis tight; hold on;
xlabel('Downrange Position (ft)');
ylabel('Altitude (ft)');

% Velocity vs time
subplot(2,2,2);
plot(t, missile.V, 'linewidth', 2);
grid on; axis tight; hold on;
xlabel('Time (sec)');
ylabel('Velocity (ft/s)');

% Flight path angle vs time
subplot(2,2,3);
plot(t, rad2deg(missile.gamma), 'linewidth', 2);
grid on; axis tight; hold on;
xlabel('Time (sec)');
ylabel('Flight Path Angle (deg)');

% Angle of attack vs time
subplot(2,2,4);
plot(t, rad2deg(missile.alpha), 'linewidth', 2);
grid on; axis tight; hold on;
xlabel('Time (sec)');
ylabel('Angle of Attack (deg)');
% -------------------------------------------------------------------------
% Title:  Missile Equations of Motion
% Author: Jordan Street
% -------------------------------------------------------------------------
% Description: This function calculates calculates all state variable
% derivatives for the missile model. It utilizes both the aero and
% atmosphere models to calculate Vdot, gammaDot, xDot and yDot.
%
% Inputs:
%     h   - altitude (ft)
%
% Outputs:
%     Vdot     - total velocity rate of change (ft/s^2)
%     gammaDot - flight path angle rate (rad/s)
%     xDot     - horizontal velocity (ft/s)
%     zDot     - downward velocity (ft/s)
% -------------------------------------------------------------------------

function [Vdot, gammaDot, xDot, zDot] = eom(V, gamma, z, alpha)
    % Vehicle parameters and constants
    S = 6;      % reference area (ft^2)
    W = 300;    % weight (lbs)
    g = 32.174; % gravity (ft/s^2)
    m = W/g;    % mass (slugs)
    
    % Calculate current dynamic pressure
    [~, rho] = atm(-z);
    Q        = 0.5*rho*V^2;
    
    % Calculate aerodynamics
    [CL, CD] = aero(alpha);
    
    % Dimensional aerodynamic forces
    L = Q*S*CL;
    D = Q*S*CD;
    
    % Velocity rate of change
    Vdot = -D/m - g*sin(gamma);
    
    % Flight path angle rate of change
    gammaDot = L/(m*V) - g/V*cos(gamma);
    
    % Position rates
    xDot =  V*cos(gamma);
    zDot = -V*sin(gamma);
end